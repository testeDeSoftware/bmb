package sample;

/**
 * Created by pedropauloduartesilva on 19/08/17.
 */
public class evosuite {

    public evosuite() {
    }

    public String[] gerarTestesUnitarios(String projectPath, String packageProject){
        String[] command = {
                "cd " + projectPath,
                "$EVOSUITE -generateTests -prefix "+ packageProject +" -Dreport_dir=evosuite-report -Dstatistics_backend=HTML -Dplot=true",
                "echo null"
        };

        return command;

    }


    public String[] executarTeste(){
        String[] command = {"",""};

        return command;
    }


    public String[] gerarCoberturaDeCodigo(){
        String[] command = {"",""};

        return command;
    }

    public String[] executarSetup(String projectPath){
        String[] command = {"cd " + projectPath,"$EVOSUITE -setup $(pwd)","echo null"};

        return command;
    }


}
