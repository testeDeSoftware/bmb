package sample;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by pedropauloduartesilva on 21/08/17.
 */

public class commands {

    private OutputStream outputStream;
    private BufferedWriter p_stdin;

    public commands(OutputStream outputStream) {
        this.outputStream = outputStream;
        p_stdin = new BufferedWriter(new OutputStreamWriter(outputStream));
    }

    public ArrayList<String> inputCommands(InputStream inputStream) {
        String s;
        ArrayList<String> bashLog = new ArrayList<String>();
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(inputStream));
        try {
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                bashLog.add(s);
                if(s.contentEquals("null")){
                    stdInput.reset();
                }
            }


        } catch (IOException e) {
            //e.printStackTrace();
        }

        return bashLog;

    }

    public void execute(String[] command){
        int n = command.length;
        for (int i = 0; i < n; i++) {
            try {
                //single execution
                System.out.println("Executando comando: " + command[i]);
                p_stdin.write(command[i]);
                p_stdin.newLine();
                p_stdin.flush();
            } catch (IOException e) {
                System.out.println("ERROR: "+ e);
            }
        }
    }

    public void finishBash(){
        System.out.println("Saindo do bash");
        System.exit(0);
        try {
            p_stdin.write("exit");
            p_stdin.newLine();
            p_stdin.flush();
        } catch (IOException e) {
            System.out.println("Erro ao finalizar aplicação: " + e);
        }
    }
}
