package sample;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by pedropauloduartesilva on 19/08/17.
 */
public class linuxBash {

    ProcessBuilder builder = new ProcessBuilder("/bin/bash");
    Process p = null;

    public OutputStream start(){

        try {
            p = builder.start();
        } catch (IOException e) {
            System.out.println(e);
        }

        return p.getOutputStream();
    }

    public InputStream logTerminal(){
        return p.getInputStream();
    }

}
