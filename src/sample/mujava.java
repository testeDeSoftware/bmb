package sample;

/**
 * Created by pedropauloduartesilva on 19/08/17.
 */
public class mujava {

    public mujava() {

    }

    public String[] executarGeradorDeMutantes(String caminhoDoProjeto, String srcProject){
        String[] command = {
                " cp -R " + caminhoDoProjeto +"/ /Users/pedropauloduartesilva/Documents/mujava/src/",
                " cp -R " + srcProject +"/ /Users/pedropauloduartesilva/Documents/mujava/src/",
                " cp -R " + caminhoDoProjeto +"/ /Users/pedropauloduartesilva/Documents/mujava/classes/",
                "cd /Users/pedropauloduartesilva/Documents/mujava/",
                "find . -name \"*.java\" > sources.txt",
                "javac @sources.txt",
                "cd /Users/pedropauloduartesilva/Documents/mujava/",
                "java mujava.gui.GenMutantsMain",
                "echo null"
        };

        return command;
    }

    public String[] executarMatadorDeMutantes(){
        String[] command = {
                "cd /Users/pedropauloduartesilva/Documents/mujava",
                "java mujava.gui.RunTestMain",
                "echo null"
        };

        return command;
    }
}
