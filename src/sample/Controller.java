package sample;



import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable{

    linuxBash lb;
    commands cm;
    evosuite es;
    mujava mj;
    @FXML
    private TextField caminhoDoProjeto;

    @FXML
    private TextField packageProject;

    @FXML
    private TextField srcProject;

    @FXML
    private TextArea bashLog;


    @Override
    public void initialize(URL location, ResourceBundle resources){
        lb = new linuxBash();
        cm = new commands(lb.start());
        es = new evosuite();
        mj = new mujava();
    }


    public void gerarSuiteDeTeste(){
        cm.execute(es.executarSetup(caminhoDoProjeto.getText()));
        bashLog.setText(bashLog.getText() +" \n " + "Realizando configurações de setup do evosuite" );
        for(String text : cm.inputCommands(lb.logTerminal())){
            bashLog.setText(bashLog.getText() +" \n " +text );
        }
        bashLog.setText(bashLog.getText() +" \n " + "Finalizando configurações de setup do evosuite" );

        cm.execute(es.gerarTestesUnitarios(caminhoDoProjeto.getText(),packageProject.getText()));
        bashLog.setText(bashLog.getText() +" \n " + "Gerando os casos de teste - Esse procedimento leva cerca de 5 a 10 minutos por pacote - Aguade" );
        for(String text : cm.inputCommands(lb.logTerminal())){
            bashLog.setText(bashLog.getText() +" \n " +text );
        }
        bashLog.setText(bashLog.getText() +" \n " + "Casos de teste Gerados com sucesso" );
    }

    public void gerarMutantes(){
        cm.execute(mj.executarGeradorDeMutantes(caminhoDoProjeto.getText(), srcProject.getText()));
        bashLog.setText(bashLog.getText() +" \n " + "Abrindo Mujava - Gerador de mutantes" );
        for(String text : cm.inputCommands(lb.logTerminal())){
            bashLog.setText(bashLog.getText() +" \n " +text );
        }
        bashLog.setText(bashLog.getText() +" \n " + "Gerador de mutantes - Processo completo" );
    }

    public void matarMutantes(){
        cm.execute(mj.executarMatadorDeMutantes());
        bashLog.setText(bashLog.getText() +" \n " + "Abrindo Mujava - Matador de mutantes" );
        for(String text : cm.inputCommands(lb.logTerminal())){
            bashLog.setText(bashLog.getText() +" \n " +text );
        }
        bashLog.setText(bashLog.getText() +" \n " + "Matador de mutantes - Processo completo" );
    }

    public void executarTodos(){

    }
}
